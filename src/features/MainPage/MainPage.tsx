import React, { useEffect } from 'react';
import {Dispatch} from 'redux'
import Header from '../../copmponents/Header';
import SearchInput from '../../copmponents/SearchInput';
import styles from './MainPage.module.css';
import RepoTable from './components/RepoTable';
import { useDispatch, useSelector } from 'react-redux';
import {getRepos, getLoading, getError, getOrganizations} from './ducks/selectors'
import {actions} from './ducks'
import Loader from '../../copmponents/Loader';
import { Typography } from '@material-ui/core';
import OrganizationTable from './components/OrganizationsTable';


const MainPage: React.FC = (): React.ReactElement =>{
  const dispatch: Dispatch = useDispatch()

  const repos = useSelector(getRepos)
  const loading = useSelector(getLoading)
  const error = useSelector(getError)
  const organizations = useSelector(getOrganizations)

  useEffect(() => {
      dispatch(actions.fetchOrganizations())
  }, [])

  const handleSearch = (text:string)=>{
    dispatch(actions.fetchRepos(text))
  }

  return (<>
      <Header text={'Добро пожаловать'} />
      <SearchInput 
        handleSearch={handleSearch} 
        lable={'Поиск по  компании'}
        lableWidth={145} 
        buttonText={'Поиск'} 
        className={styles.search}  
      />
      { loading && <Loader/>}
      {!loading && !!repos.length && <RepoTable className={styles.table} paginationClassName={styles.table} rows={repos}/>}
      { error&& <Typography variant='h5' className={styles.error} >{error} </Typography>}
      {!loading && error && <OrganizationTable rows={organizations} className={styles.table} paginationClassName={styles.table}  />}
      </>
  );
}

export default MainPage;