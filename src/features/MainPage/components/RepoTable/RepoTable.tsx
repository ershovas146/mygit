import React from 'react'
import { TableContainer, Table, TablePagination } from '@material-ui/core';
import CustomTableHead from '../../../../copmponents/CustomTableHead';
import {headers} from '../../constants'
import CustomTableBody from '../../../../copmponents/CustomTableBody';

export type RepoTableProps={
    rows: Array<Array<string|number>>
    className?: string,
    paginationClassName?: string,
}

const RepoTable: React.FC<RepoTableProps> = ({className,paginationClassName,rows}):React.ReactElement =>{
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = ( event: React.MouseEvent<HTMLButtonElement, MouseEvent> | null,newPage:number) => {
        setPage(newPage);
      };
    
    const handleChangeRowsPerPage = ( event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
      };
    return(
        <TableContainer >
            <Table
                size={'medium'}
                className={className}
            >
                <CustomTableHead lables={headers} />
                <CustomTableBody records={rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}/>
            </Table>
            <TablePagination
                    rowsPerPageOptions={[10, 15, 25, 50]}
                    component="div" 
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    className={paginationClassName}
                />
        </TableContainer>
    )
}

export default RepoTable;