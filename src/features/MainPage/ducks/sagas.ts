import { Repo } from './index';
import { call, put, takeLatest } from 'redux-saga/effects'
import { actions } from '../ducks'
import axios from 'axios'


export function* fetchRepos(action: ReturnType<typeof actions.fetchRepos>) {
    try {
  
        const data:Array<Repo>|{message: string} = yield call( () => axios.get(`https://api.github.com/orgs/${action.payload}/repos`).then( (response : any)=> {
            return response.data;
          })
          .catch(function (error) {
            if (error.response.status === 404){
            return error.response.data;
            }else{
                console.log(error)
            }
          }))
      if ((<Array<Repo>>data).length) {
        yield put(actions.fetchReposSuccses(<Array<Repo>>data))
      } else {
        yield put(actions.fetchReposNotFound(<{message: string}>data))
      }
    } catch (e) {
      yield put(actions.fetchReposError())
    }
  }
export function* fetchOrganizations(action: ReturnType<typeof actions.fetchRepos>) {
    try {
        const data:Array<{login: string}>= yield call( () => axios.get('https://api.github.com/organizations').then( (response : any)=> {
            return response.data;
          })
          .catch(function (error) {
                console.log(error)
            }
        ))
        yield put(actions.fetchOrganizationsSuccses(data))
    } catch (e) {
      yield put(actions.fetchOrganizationsError())
    }
  }

export default function* actionWatcher() {
    yield takeLatest(actions.fetchRepos, fetchRepos)
    yield takeLatest(actions.fetchOrganizations.type, fetchOrganizations)
    
  }