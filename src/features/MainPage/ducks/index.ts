import { createSlice, PayloadAction, Draft } from '@reduxjs/toolkit'

export type Repo = {
    name: string,
    html_url: string,
    forks_count: number,
    watchers_count: number,
    stargazers_count: number,
}

export type ReposState ={
    repos: Array<Repo>,
    isLoading: boolean,
    errorMassage: string|null
    organizations: Array<{login: string}>
}

const initialState:ReposState ={
    repos: [],
    isLoading: false,
    errorMassage: null,
    organizations: []
}
export const reposSlice = createSlice({
    name: 'repo',
    initialState,
    reducers:{
        fetchOrganizations: (state: Draft<ReposState>): void =>{
            state.organizations =[]
        },
        fetchOrganizationsSuccses: (state: Draft<ReposState>, action:PayloadAction<Array<{login: string}>>): void =>{
            state.organizations = action.payload
            state.isLoading = false
        },
        fetchOrganizationsError:(state: Draft<ReposState>): void =>{
        },
        fetchRepos: (state: Draft<ReposState>, _action:PayloadAction<string>): void =>{
            state.isLoading = true
            state.errorMassage = null
            state.repos =[]
        },
        fetchReposNotFound:(state: Draft<ReposState>, action:PayloadAction<{message:string}>):void =>{
            state.errorMassage = action.payload.message
            state.isLoading = false
        },
        fetchReposSuccses: (state: Draft<ReposState>, action:PayloadAction<Array<Repo>>): void =>{
            state.repos = action.payload
            state.isLoading = false
        },
        fetchReposError: (state: Draft<ReposState>): void =>{
            state.isLoading = false
        }
    }
})

export const { actions, reducer } = reposSlice

export default reducer