import React from 'react'
import {TableRow, TableCell, TableBody, ListItem } from '@material-ui/core'
import styles from './CustomTableBody.module.css'
import { url } from 'inspector'

export type TableBodyProps= {
    records: Array<Array<string|number>>
}

const CustomTableBody: React.FC<TableBodyProps> = ({records}):React.ReactElement => {
    return(
        <TableBody className={styles.tableBody}>
          { records.map((row, indexRow) => (
            <TableRow key={indexRow}>
               {row.map((item, indexItem)=>(
               <TableCell align='left' key={indexItem}>
                   {typeof item === 'string' && item.split(':')[0]==='https' ? <a href={item}>{item}</a>: item}
               </TableCell>
               ))}
            </TableRow>
          ))}
        </TableBody>
    )
}

export default CustomTableBody